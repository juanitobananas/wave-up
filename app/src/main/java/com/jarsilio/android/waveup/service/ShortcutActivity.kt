package com.jarsilio.android.waveup.service

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.jarsilio.android.waveup.MainActivity
import com.jarsilio.android.waveup.REQUEST_LOCK_PERMISSIONS_ACTION
import timber.log.Timber

class ShortcutActivity : AppCompatActivity() {
    val state: WaveUpWorldState by lazy { WaveUpWorldState.getInstance(this) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        when (intent?.action) {
            LOCK_NOW_ACTION -> {
                Timber.d("User tapped on 'Lock now' shortcut.")
                if (!state.isLockScreenAdmin && !state.isAccessibilityServiceEnabled) {
                    Timber.d("Opening MainActivity to request permissions")
                    val showLockPermissionsDialogIntent = Intent(this, MainActivity::class.java)
                    showLockPermissionsDialogIntent.action = REQUEST_LOCK_PERMISSIONS_ACTION
                    startActivity(showLockPermissionsDialogIntent)
                } else {
                    Timber.d("Locking device...")
                    ScreenHandler.getInstance(this).turnOffScreen(0)
                }
            }
        }

        finish()
    }

    companion object {
        const val LOCK_NOW_ACTION = "com.jarsilio.android.waveup.action.LOCK_NOW"
    }
}
