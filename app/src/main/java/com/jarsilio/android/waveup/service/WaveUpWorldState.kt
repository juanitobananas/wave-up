/*
 * Copyright (c) 2016-2019 Juan García Basilio
 *
 * This file is part of WaveUp.
 *
 * WaveUp is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * WaveUp is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with WaveUp.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.jarsilio.android.waveup.service

import android.Manifest
import android.annotation.SuppressLint
import android.annotation.TargetApi
import android.app.ActivityManager
import android.app.AppOpsManager
import android.app.admin.DevicePolicyManager
import android.app.usage.UsageStatsManager
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothHeadset
import android.content.ComponentName
import android.content.Context
import android.content.pm.PackageManager
import android.content.res.Configuration
import android.os.Build
import android.os.PowerManager
import androidx.core.content.ContextCompat
import com.jarsilio.android.common.extensions.isLollipopOrNewer
import com.jarsilio.android.common.utils.SingletonHolder
import com.jarsilio.android.waveup.model.AppDatabase
import com.jarsilio.android.waveup.receivers.CallStateReceiver
import com.jarsilio.android.waveup.receivers.HeadsetStateReceiver
import com.jarsilio.android.waveup.receivers.LockScreenAdminReceiver
import timber.log.Timber

class WaveUpWorldState private constructor(context: Context) {
    private val applicationContext: Context = context.applicationContext

    val isScreenOn: Boolean
        get() {
            val powerManager = applicationContext.getSystemService(Context.POWER_SERVICE) as PowerManager

            return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT_WATCH) {
                powerManager.isInteractive
            } else {
                powerManager.isScreenOn
            }
        }

    val isPortrait: Boolean
        get() {
            val orientation = applicationContext.resources.configuration.orientation
            return orientation == Configuration.ORIENTATION_PORTRAIT || orientation == Configuration.ORIENTATION_UNDEFINED
        }

    val isOngoingCall: Boolean
        get() = CallStateReceiver.isOngoingCall

    val isHeadsetConnected: Boolean
        get() = HeadsetStateReceiver.isHeadsetPlugged

    val isBluetoothHeadsetConnected: Boolean
        @SuppressLint("MissingPermission")
        get() {
            return if (isBluetoothPermissionGranted) {
                val bluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
                bluetoothAdapter != null && bluetoothAdapter.isEnabled &&
                    bluetoothAdapter.getProfileConnectionState(BluetoothHeadset.HEADSET) == BluetoothHeadset.STATE_CONNECTED
            } else {
                false
            }
        }

    val isBluetoothPermissionGranted: Boolean
        get() {
            return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
                val permissionCheck = ContextCompat.checkSelfPermission(applicationContext, Manifest.permission.BLUETOOTH_CONNECT)
                permissionCheck == PackageManager.PERMISSION_GRANTED
            } else {
                true
            }
        }
    val isPhonePermissionGranted: Boolean
        get() {
            val permissionCheck = ContextCompat.checkSelfPermission(applicationContext, Manifest.permission.READ_PHONE_STATE)
            return permissionCheck == PackageManager.PERMISSION_GRANTED
        }

    val isLockScreenAdmin: Boolean
        get() {
            val devicePolicyManager: DevicePolicyManager =
                applicationContext.getSystemService(
                    Context.DEVICE_POLICY_SERVICE,
                ) as DevicePolicyManager
            val adminReceiver = ComponentName(applicationContext, LockScreenAdminReceiver::class.java)
            return devicePolicyManager.isAdminActive(adminReceiver)
        }

    val isIgnoringBatteryOptimizations: Boolean
        get() {
            return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                val powerManager = applicationContext.getSystemService(Context.POWER_SERVICE) as PowerManager
                powerManager.isIgnoringBatteryOptimizations(applicationContext.packageName)
            } else {
                true
            }
        }

    val isAccessibilityServiceEnabled: Boolean
        get() {
            val expectedLockAccessibilityService = "${applicationContext.packageName}/${LockAccessibilityService::class.java.canonicalName}"

            val enabledAccessibilityServices: String? =
                android.provider.Settings.Secure.getString(
                    applicationContext.contentResolver,
                    android.provider.Settings.Secure.ENABLED_ACCESSIBILITY_SERVICES,
                )
            // Timber.v("Enabled accessibility services: $enabledAccessibilityServices")

            if (enabledAccessibilityServices != null) {
                for (accessibilityService in enabledAccessibilityServices.split(":")) {
                    if (accessibilityService.equals(expectedLockAccessibilityService, ignoreCase = true)) {
                        return true
                    }
                }
            }
            return false
        }

    val isUsageAccessAllowed: Boolean

        @TargetApi(Build.VERSION_CODES.LOLLIPOP)
        get() {
            return if (isLollipopOrNewer) {
                try {
                    val packageManager = applicationContext.packageManager
                    val applicationInfo = packageManager.getApplicationInfo(applicationContext.packageName, 0)
                    val appOpsManager = applicationContext.getSystemService(Context.APP_OPS_SERVICE) as AppOpsManager
                    val mode =
                        appOpsManager.checkOpNoThrow(
                            AppOpsManager.OPSTR_GET_USAGE_STATS,
                            applicationInfo.uid,
                            applicationInfo.packageName,
                        )
                    mode == AppOpsManager.MODE_ALLOWED
                } catch (e: PackageManager.NameNotFoundException) {
                    false
                }
            } else {
                true
            }
        }

    fun isExcludedAppInForeground(): Boolean {
        val appInForeground = getForegroundPackageName()
        for (app in AppDatabase.getInstance(applicationContext).appsDao().excludedApps) {
            if (app.packageName == appInForeground) {
                Timber.d("${app.name} (${app.packageName}) is running in foreground")
                return true
            }
        }
        return false
    }

    private fun getForegroundPackageName(): String {
        var lastUsedPackage = ""
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            @SuppressLint("WrongConstant")
            val usageStatsManager = applicationContext.getSystemService("usagestats") as UsageStatsManager?
            if (usageStatsManager == null) {
                Timber.e("Failed to get UsageStatsManager. This should not be a problem unless the 'exluded apps' list is in use.")
                return ""
            }

            val now = System.currentTimeMillis()
            val usageStatsList = usageStatsManager.queryUsageStats(UsageStatsManager.INTERVAL_DAILY, now - 1000 * 1000, now)
            var lastTimeUsed: Long = 0
            for (usageStats in usageStatsList) {
                if (usageStats.lastTimeUsed > lastTimeUsed) {
                    lastTimeUsed = usageStats.lastTimeUsed
                    lastUsedPackage = usageStats.packageName
                }
            }
            Timber.v("Most recently run package according to UsageStats (>= LOLLIPOP): $lastUsedPackage")
        } else {
            val activityManager = applicationContext.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
            val tasks = activityManager.runningAppProcesses
            if (tasks != null && tasks[0] != null) {
                lastUsedPackage = tasks[0].processName
            } else {
                Timber.e("Couldn't read recent running apps using ActivityManager")
            }
            Timber.v("Most recently run package according to ActivityManager (< LOLLIPOP): $lastUsedPackage")
        }
        return lastUsedPackage
    }

    override fun toString(): String {
        return "{ " +
            "isScreenOn: $isScreenOn, " +
            "isPortrait: $isPortrait, " +
            "isOngoingCall: $isOngoingCall, " +
            "isLockScreenAdmin: $isLockScreenAdmin, " +
            "isIgnoringBatteryOptimizations: $isIgnoringBatteryOptimizations, " +
            "isUsageAccessAllowed: $isUsageAccessAllowed, " +
            "isHeadsetConnected: $isHeadsetConnected, " +
            "isBluetoothHeadsetConnected: $isBluetoothHeadsetConnected, " +
            "isPhonePermissionGranted: $isPhonePermissionGranted, " +
            "isAccessibilityServiceEnabled: $isAccessibilityServiceEnabled" +
            " }"
    }

    companion object : SingletonHolder<WaveUpWorldState, Context>(::WaveUpWorldState)
}
