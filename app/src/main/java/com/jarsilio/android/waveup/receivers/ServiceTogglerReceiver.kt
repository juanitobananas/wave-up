package com.jarsilio.android.waveup.receivers

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.jarsilio.android.waveup.prefs.Settings
import com.jarsilio.android.waveup.service.WaveUpService
import timber.log.Timber

const val ACTION_WAVEUP_ENABLE = "com.jarsilio.android.waveup.intent.action.WAVEUP_ENABLE"
const val ACTION_WAVEUP_DISABLE = "com.jarsilio.android.waveup.intent.action.WAVEUP_DISABLE"

class ServiceTogglerReceiver : BroadcastReceiver() {
    override fun onReceive(
        context: Context?,
        intent: Intent?,
    ) {
        if (context == null) {
            Timber.e("Received broadcast from third-party app with null context. Can't start or stop WaveUp")
            return
        } else if (intent == null) {
            Timber.e("Received broadcast from third-party app with null intent. Can't start or stop     WaveUp")
            return
        }

        when (intent.action) {
            ACTION_WAVEUP_ENABLE -> {
                Timber.d("Received ACTION_WAVEUP_ENABLE from third-party app. Enabling WaveUp.")
                Settings.getInstance(context).isServiceEnabled = true
                WaveUpService.start(context)
            }
            ACTION_WAVEUP_DISABLE -> {
                Timber.d("Received ACTION_WAVEUP_DISABLE from third-party app. Disabling WaveUp.")
                Settings.getInstance(context).isServiceEnabled = false
                WaveUpService.stop(context)
            }
        }
    }
}
