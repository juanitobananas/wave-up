package com.jarsilio.android.waveup.receivers

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import timber.log.Timber

class HeadsetStateReceiver : BroadcastReceiver() {
    override fun onReceive(
        context: Context,
        intent: Intent,
    ) {
        if (intent.action == Intent.ACTION_HEADSET_PLUG) {
            when (intent.getIntExtra("state", -1)) {
                0 -> {
                    Timber.d("Headset unplugged")
                    isHeadsetPlugged = false
                }
                1 -> {
                    Timber.d("Headset plugged")
                    isHeadsetPlugged = true
                }
                else -> Timber.e("Unknown headset plugged state. Leaving as is: $isHeadsetPlugged")
            }
        }
    }

    companion object {
        // Unfortunately I cannot read this live, so I'll just assume there is no ongoing call the first time
        var isHeadsetPlugged = false
            private set
    }
}
