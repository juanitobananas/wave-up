package com.jarsilio.android.waveup.model

import android.content.Context
import android.content.pm.PackageManager
import android.graphics.drawable.Drawable
import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Database
import androidx.room.Delete
import androidx.room.Entity
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.PrimaryKey
import androidx.room.Query
import androidx.room.Room
import androidx.room.RoomDatabase
import com.jarsilio.android.common.utils.SingletonHolder
import timber.log.Timber

@Entity
data class App(
    @PrimaryKey var packageName: String,
    var name: String,
    var isSystem: Boolean,
    var isExcluded: Boolean,
) {
    fun getIcon(context: Context): Drawable? {
        return try {
            context.packageManager.getApplicationIcon(packageName)
        } catch (e: PackageManager.NameNotFoundException) {
            Timber.e(e)
            null
        }
    }
}

@Dao
interface BaseDao<in T> {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertIfNotExists(t: T)

    @Delete
    fun delete(t: T)
}

@Dao
interface AppsDao : BaseDao<App> {
    @get:Query("SELECT * FROM app ORDER BY name COLLATE UNICODE")
    val all: List<App>

    @get:Query("SELECT * FROM app WHERE isExcluded = 1 ORDER BY name COLLATE UNICODE")
    val excludedApps: List<App>

    @get:Query("SELECT * FROM app WHERE isExcluded = 1 ORDER BY name COLLATE UNICODE")
    val excludedAppsLive: LiveData<List<App>>

    @get:Query("SELECT * FROM app WHERE isExcluded = 0 ORDER BY name COLLATE UNICODE")
    val notExcludedAppsLive: LiveData<List<App>>

    @Query("SELECT * FROM app WHERE packageName LIKE :packageName LIMIT 1")
    fun loadByPackageName(packageName: String): App?

    @Query("UPDATE App SET isExcluded = :isExcluded WHERE packageName = :packageName")
    fun setExcluded(
        packageName: String,
        isExcluded: Boolean,
    )
}

@Database(entities = [App::class], version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun appsDao(): AppsDao

    companion object : SingletonHolder<AppDatabase, Context>({
        Room.databaseBuilder(it.applicationContext, AppDatabase::class.java, "Apps").build()
    })
}
