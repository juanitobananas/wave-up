package com.jarsilio.android.waveup

import android.app.Activity
import android.app.admin.DevicePolicyManager
import android.content.ComponentName
import android.content.Intent
import android.net.Uri
import android.provider.Settings
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import com.jarsilio.android.common.extensions.isPieOrNewer
import com.jarsilio.android.waveup.extensions.isInstalledOnSdCard
import com.jarsilio.android.waveup.extensions.settings
import com.jarsilio.android.waveup.extensions.state
import com.jarsilio.android.waveup.receivers.LockScreenAdminReceiver
import timber.log.Timber

class PermissionsHandler(private val activity: Activity) {
    fun openPhonePermissionExplanationIfNecessary() {
        if (activity.state.isPhonePermissionGranted) {
            Timber.d("Not opening phone permissions dialog (permission already granted).")
            return
        }

        Timber.d("Showing dialog to see if the user wants to request Android's 'Phone Permission '")
        AlertDialog.Builder(activity).apply {
            setMessage(R.string.phone_permission_explanation)
            setPositiveButton(R.string.phone_permission_yes) { _, _ ->
                Timber.d("Requesting Android's 'Phone Permission'")
                ActivityCompat.requestPermissions(activity, READ_PHONE_STATE_PERMISSION_ARRAY, READ_PHONE_STATE_PERMISSION_REQUEST_CODE)
            }
            setNegativeButton(R.string.phone_permission_no) { _, _ ->
                Timber.d("User decided *not* to request 'Phone Permission'")
            }
            show()
        }
    }

    fun openBluetoothPermissionExplanationIfNecessary() {
        if (activity.state.isBluetoothPermissionGranted) {
            Timber.d("Not opening bluetooth permissions dialog (permission already granted).")
            return
        }
        Timber.d("Showing dialog to see if the user wants to request Android's 'Bluetooth Permission '")
        AlertDialog.Builder(activity).apply {
            setMessage(R.string.bluetooth_permission_explanation)
            setPositiveButton(R.string.phone_permission_yes) { _, _ ->
                Timber.d("Requesting Android's 'Bluetooth Permission'")
                ActivityCompat.requestPermissions(activity, BLUETOOTH_STATE_PERMISSION_ARRAY, 0)
            }
            setNegativeButton(R.string.phone_permission_no) { _, _ ->
                Timber.d("User decided *not* to request 'Bluetooth Permission'")
            }
            show()
        }
    }

    fun openLockDevicePermissionExplanationIfNecessary() {
        if (isPieOrNewer) {
            openAccessibilitySettingsExplanationIfNecessary()
        } else {
            if (activity.isInstalledOnSdCard) {
                Timber.d("App in installed on SD-card. Can't obtain device admin privileges.")
                activity.settings.isLockScreen = false
                openInstalledOnSdCardExplanation()
            } else {
                openDeviceAdminRightsExplanationIfNecessary()
            }
        }
    }

    private fun openAccessibilitySettingsExplanationIfNecessary() {
        if (activity.state.isAccessibilityServiceEnabled) {
            Timber.d("Not opening accessibility settings dialog (permission already granted).")
            return
        }

        Timber.d("Showing dialog to see if the user wants to open Android's 'Accessibility Settings'")
        AlertDialog.Builder(activity).apply {
            setMessage(R.string.accessibility_service_explanation)
            setPositiveButton(android.R.string.ok) { _, _ ->
                Timber.d("Opening Android's 'Accessibility Settings' activity")
                activity.startActivityForResult(Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS), ACCESSIBILITY_SERVICE_REQUEST_CODE)
            }
            setNegativeButton(android.R.string.cancel) { _, _ ->
                Timber.d("User decided *not* to open 'Accessibility Settings' activity")
                context.settings.isLockScreen = false
            }
            setCancelable(false)
            show()
        }
    }

    private fun openInstalledOnSdCardExplanation() {
        Timber.d("Showing dialog to explaining that the aüü can't lock the device if it's installed to the sd card'")

        AlertDialog.Builder(activity).apply {
            setMessage(R.string.sd_card_lock_admin_rights_not_possible_explanation)
            setPositiveButton(android.R.string.ok) { _, _ -> }
            setCancelable(false)
            show()
        }
    }

    private fun openDeviceAdminRightsExplanationIfNecessary() {
        if (activity.state.isLockScreenAdmin) {
            Timber.d("Not opening device admin dialog (permission already granted).")
            return
        }

        AlertDialog.Builder(activity).apply {
            setMessage(R.string.lock_admin_rights_explanation)
            setPositiveButton(android.R.string.ok) { _, _ ->
                run {
                    Timber.d("Opening Android's 'Device Admin' activity")
                    val lockScreenAdminComponentName = ComponentName(activity, LockScreenAdminReceiver::class.java)
                    val intent = Intent(DevicePolicyManager.ACTION_ADD_DEVICE_ADMIN)
                    intent.putExtra(DevicePolicyManager.EXTRA_DEVICE_ADMIN, lockScreenAdminComponentName)
                    intent.putExtra(DevicePolicyManager.EXTRA_ADD_EXPLANATION, R.string.lock_admin_rights_explanation)
                    activity.startActivityForResult(intent, DEVICE_ADMIN_REQUEST_CODE)
                }
            }
            setNegativeButton(android.R.string.cancel) { _, _ ->
                run {
                    Timber.d("User decided *not* to open 'Device Admin' activity")
                    context.settings.isLockScreen = false
                }
            }
            setCancelable(false)
            show()
        }
    }

    fun openUsageAccessExplanationIfNecessary() {
        if (activity.state.isUsageAccessAllowed) {
            Timber.d("Usage access already granted. Opening activity to choose apps to exclude.")
            activity.startActivity(Intent(activity, ExcludeAppsActivity::class.java))
            return
        }

        AlertDialog.Builder(activity).apply {
            setMessage(R.string.usage_access_explanation)
            setPositiveButton(android.R.string.ok) { _, _ ->
                Timber.d("Opening Android's 'Usage Access' activity")
                activity.startActivity(Intent(Settings.ACTION_USAGE_ACCESS_SETTINGS))
            }
            setNegativeButton(android.R.string.cancel) { _, _ ->
                Timber.d("User decided *not* to open 'Usage Access' settings")
            }
            setCancelable(false)
            show()
        }
    }

    fun requestIgnoreBatteryOptimizationsIfNecessary() {
        if (!activity.settings.isShowNotification && !activity.state.isIgnoringBatteryOptimizations) {
            Timber.d("Requesting to ignore battery optimizations for WaveUp")
            activity.startActivityForResult(
                Intent(
                    Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS,
                    Uri.parse("package:${activity.packageName}"),
                ),
                BATTERY_OPTIMIZATION_REQUEST_CODE,
            )
        }
    }

    companion object {
        const val DEVICE_ADMIN_REQUEST_CODE = 100
        const val READ_PHONE_STATE_PERMISSION_REQUEST_CODE = 300
        const val BATTERY_OPTIMIZATION_REQUEST_CODE = 400
        const val ACCESSIBILITY_SERVICE_REQUEST_CODE = 500

        private val READ_PHONE_STATE_PERMISSION_ARRAY = arrayOf("android.permission.READ_PHONE_STATE")
        private val BLUETOOTH_STATE_PERMISSION_ARRAY = arrayOf("android.permission.BLUETOOTH_CONNECT")
    }
}
