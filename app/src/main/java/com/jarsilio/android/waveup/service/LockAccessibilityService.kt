package com.jarsilio.android.waveup.service

import android.accessibilityservice.AccessibilityService
import android.annotation.TargetApi
import android.os.Build
import android.view.accessibility.AccessibilityEvent
import com.jarsilio.android.common.extensions.isPieOrNewer
import com.jarsilio.android.waveup.R
import timber.log.Timber

@TargetApi(Build.VERSION_CODES.P)
class LockAccessibilityService : AccessibilityService() {
    override fun onAccessibilityEvent(event: AccessibilityEvent?) {
        Timber.v("onAccessibilityEvent(): $event")
        event?.let {
            if (event.eventType == AccessibilityEvent.TYPE_NOTIFICATION_STATE_CHANGED &&
                event.text?.get(0) == getString(R.string.accessibility_service_text)
            ) {
                Timber.d("Received WaveUp Lock Command")
                lockDevice()
            }
        }
    }

    private fun lockDevice() {
        if (isPieOrNewer) {
            Timber.d("Locking device using AccessibilityService")
            performGlobalAction(GLOBAL_ACTION_LOCK_SCREEN)
        } else {
            // This should never be reached, but we'll log it just in case.
            Timber.e("Locking device using AccessibilityService is only available for Android versions >= Pie")
        }
    }

    override fun onInterrupt() {
        Timber.v("onInterrupt()")
    }
}
