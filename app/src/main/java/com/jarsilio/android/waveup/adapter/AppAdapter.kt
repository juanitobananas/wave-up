package com.jarsilio.android.waveup.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.jarsilio.android.waveup.R
import com.jarsilio.android.waveup.model.App
import com.jarsilio.android.waveup.model.AppDatabase
import timber.log.Timber

class AppListAdapter : ListAdapter<App, AppHolder>(AppDiffCallback()) {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int,
    ): AppHolder {
        val appCardView =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.app_card, parent, false) as CardView
        return AppHolder(appCardView)
    }

    override fun onBindViewHolder(
        holder: AppHolder,
        position: Int,
    ) {
        holder.updateWithApp(getItem(position))
    }
}

class AppHolder(view: View) : RecyclerView.ViewHolder(view) {
    private val icon = view.findViewById<View>(R.id.app_icon) as ImageView
    private val appName = view.findViewById<View>(R.id.app_name) as TextView
    private val packageName = view.findViewById<View>(R.id.package_name) as TextView
    private val cardView = view.findViewById<View>(R.id.card_view) as CardView
    private val dao = AppDatabase.getInstance(view.context).appsDao()

    fun updateWithApp(app: App) {
        icon.setImageDrawable(app.getIcon(cardView.context))
        appName.text = app.name
        packageName.text = app.packageName

        if (app.isSystem) {
            packageName.setTextColor(ContextCompat.getColor(cardView.context, R.color.darkBlue))
        }

        // Adding click listener on CardView to open clicked application directly from here .
        cardView.setOnClickListener {
            Timber.d("Clicked on ${app.packageName}.")

            if (app.isExcluded) {
                Timber.d("${app.packageName} deselected. It is *not* excluded from WaveUp lock anymore")
                Thread {
                    dao.setExcluded(app.packageName, false)
                }.start()
            } else {
                Timber.d("${app.packageName} selected. It is now excluded from WaveUp lock")
                Thread {
                    dao.setExcluded(app.packageName, true)
                }.start()
            }
        }
    }
}

class AppDiffCallback : DiffUtil.ItemCallback<App>() {
    override fun areItemsTheSame(
        oldItem: App,
        newItem: App,
    ): Boolean {
        return oldItem.packageName == newItem.packageName
    }

    override fun areContentsTheSame(
        oldItem: App,
        newItem: App,
    ): Boolean {
        return oldItem == newItem
    }
}
