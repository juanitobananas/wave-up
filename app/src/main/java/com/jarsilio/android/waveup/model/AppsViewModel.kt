package com.jarsilio.android.waveup.model

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel

class AppsViewModel : ViewModel() {
    fun getExcludedApps(appsDao: AppsDao): LiveData<List<App>> {
        return appsDao.excludedAppsLive
    }

    fun getNotExcludedApps(appsDao: AppsDao): LiveData<List<App>> {
        return appsDao.notExcludedAppsLive
    }
}
