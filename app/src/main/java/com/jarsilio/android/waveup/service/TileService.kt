/*
 * Copyright (c) 2016-2019 Juan García Basilio
 *
 * This file is part of WaveUp.
 *
 * WaveUp is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * WaveUp is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with WaveUp.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.jarsilio.android.waveup.service

import android.annotation.TargetApi
import android.os.Build
import android.service.quicksettings.Tile
import android.service.quicksettings.TileService
import com.jarsilio.android.waveup.extensions.settings
import timber.log.Timber

@TargetApi(Build.VERSION_CODES.N)
class TileService : TileService() {
    override fun onClick() {
        super.onClick()
        Timber.d("Clicked on tile")
        settings.isServiceEnabled = !settings.isServiceEnabled
        WaveUpService.start(this) // This will actually stop the service if it is not enabled
        updateTileActiveness()
    }

    override fun onStartListening() {
        super.onStartListening()
        Timber.d("Tile became visible. Setting its 'enabledness'")
        updateTileActiveness()
    }

    private fun updateTileActiveness() {
        if (qsTile == null) {
            Timber.e("qsTile is null. qsTile is only valid for updates between onStartListening() and onStopListening().")
            return
        }

        qsTile.state =
            if (settings.isServiceEnabled) {
                Timber.d("WaveUp is enabled. Setting Tile.STATE_ACTIVE")
                Tile.STATE_ACTIVE
            } else {
                Timber.d("WaveUp is disabled. Setting Tile.STATE_INACTIVE")
                Tile.STATE_INACTIVE
            }
        qsTile.updateTile()
    }
}
