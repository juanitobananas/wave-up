<resources>
    <string name="prefs_service">Service</string>
    <string name="pref_enable">Activer</string>

    <string name="prefs_modes">Modes de WaveUp</string>
    <string name="pref_wave_mode">Mode \"mouvement\"</string>
    <string name="pref_wave_mode_summary">Allumer l’écran d’un mouvement au dessus du capteur de proximité.</string>
    <string name="pref_pocket_mode">Mode poche</string>
    <string name="pref_pocket_mode_summary">Allumer l\'ecran en sortant le téléphone d\'une poche ou d\'un sac</string>

    <string name="pref_lock_screen">Verrouiller l\'écran </string>
    <string name="pref_lock_screen_mode_summary">Éteindre et verrouiller l\'écran lorsque le capteur de proximité est recouvert.</string>
    <string name="pref_lock_screen_when_landscape">Verrouiller en mode paysage</string>
    <string name="pref_lock_screen_when_landscape_summary">Désactiver cette option pour empêcher le verrouillage quand le téléphone est horizontal, lors d\'un visionnage vidéo par exemple.</string>
    <string name="pref_lock_screen_with_power_button_as_root">Contournement pour l\'empreinte et Smart Lock.</string>
    <string name="pref_lock_screen_with_power_button_as_root_summary">Éteindre l\'écran en simulant un appui sur le bouton Marche/Arrêt. Nécessite l\'accès root.</string>
    <string name="pref_lock_screen_vibrate_on_lock">Vibrer avant le  verrouillage</string>
    <string name="pref_lock_screen_vibrate_on_lock_summary">Être notifié d\'une détection du capteur de proximité, pour éviter de verrouiller le téléphone accidentellement.</string>
    <string name="pref_lock_screen_app_exception">Applis exclues</string>
    <string name="pref_lock_screen_app_exception_summary">Empêchez le verrouillage si l\'une de ces applications est au premier plan. Tapez pour ouvrir et modifier la liste.</string>
    <string name="pref_sensor_cover_time_before_locking_screen">Temps avant verrouillage lors du recouvrement</string>
    <string name="pref_sensor_cover_time_before_locking_screen_summary">Période de temps pendant laquelle le capteur de proximité doit être recouvert pour verrouiller l\'écran. Actuellement: %s.</string>
    <string name="pref_notification_section">Notification</string>
    <string name="pref_show_notification">Voir notification</string>
    <string name="pref_show_notification_summary">Afficher une notification permanente pour gérer WaveUp et s\'assurer qu\'il reste actif.</string>
    <!-- From Pie onwards, we just show the Android system settings and always use a ForegroundService -->
    <string name="pref_show_notification_v28">Paramètres de notification</string>
    <string name="pref_show_notification_summary_v28">Appuyer ici pour ouvrir les paramètres de notification d\'Android pour WaveUp.\n\n<small>Note du développeur : à partir d\'Android 9, les applications qui lisent les capteurs en arrière-plan sont <b>obligées</b> d\'afficher une notification permanente .  Toutefois, vous pouvez la masquer à l\'aide des options système. Selon votre appareil, cela peut ne pas fonctionner. Dans ce cas, redémarrez-le et réessayer.</small></string>

    <string name="wave_up_service_started">WaveUp est actif</string>
    <string name="wave_up_service_stopped">WaveUp n\'est pas actif</string>
    <string name="tap_to_open">Sélectionnez afin d\'ouvrir WaveUp</string>
    <string name="pause">Pause</string>
    <string name="resume">Reprendre l\'activité</string>
    <string name="disable">Stop</string>

    <string name="lock_admin_rights_explanation"><b>Verrouiller appareil : Administrateur appareil</b>

\n\nAfin de verrouiller l\'écran, nous devons configurer WaveUp en tant que programme Administrateur d\'appareil.

\n\nUne fois une application passée Administrateur d\'appareil, vous ne pourrez la désinstaller normalement qu\'après lui avoir retirée la permission Administrateur d\'appareil.

\n\nSi vous souhaitez retirer la permission Administrateur d\'appareil, allez simplement dans <i>Paramètres -> Sécurité -> Administrateurs d\'appareil</i> et décochez WaveUp.

\n\nSi vous souhaitez désinstaller WaveUp, appuyez simplement sur le bouton \"Désinstaller WaveUp\" en bas de l\'application. Cela retirera les privilèges Administrateur d\'appareil <i>et</i> désinstallera directement WaveUp.

\n\nÊtes-vous sûr de vouloir configurer WaveUp en tant qu\'Administrateur d\'appareil ?</string>
    <string name="accessibility_service_explanation">
<b>Verrouiller l\'appareil : Service d\'accessibilité</b>

\n\nAfin de pouvoir verrouiller l\'écran, nous devons configurer WaveUp en tant \"Service d\'accessibilité\"

\n\nAccorder les droits Service d\'accessibilité à une application lui permet d\'espionner l\'utilisation de l\'appareil si souhaité. Cela permet aussi à une application d\'utiliser des fonctions spécifiques qui ne seraient pas accessibles autrement. Par exemple, WaveUp utilise le Service d\'accessibilité <i>GLOBAL_ACTION_LOCK_SCREEN</i> afin de verrouiller l\'écran.

\n\n<b>Veuillez-y penser à deux fois avant de donner votre confiance à une application !</b>

\n\nWaveUp est un logiciel open source, vous êtes invité à vérifier le code (ou à demander à un ami de le faire) afin d\'être sûr que nous ne faisons rien de mal (nous promettons que non mais vous ne devriez pas nous croire sur parole).

\n\nVoulez-vous configurer WaveUp en tant que Service d\'accessibilité ? Vous devrez accorder les privilèges manuellement au prochain écran.</string>
    <string name="sd_card_lock_admin_rights_not_possible_explanation">
<b>Erreur : WaveUp installé sur la carte SD</b>

\n\nMalheureusement, il est impossible pour les applications installées sur la carte SD de demander des privilèges administrateurs, privilèges nécessaires afin de pouvoir verrouiller l\'appareil.

\n\n<b>Veuillez placer WaveUp dans la partition interne et réessayer.</b></string>

    <string name="something_went_wrong">Erreur</string>
    <string name="lock_disabled_warning_device_admin_text">WaveUp a dû désactiver l\'option \"verrouiller\" à cause de privilèges <i>Administrateur d\'appareil</i> manquants.

\n\nVeuillez réactiver l\'option et configurer WaveUp en tant qu\'<i>Administrateur appareil</i> une fois demandé.

\n\nSi cela ne marche pas, veuillez activer WaveUp dans <i>Paramètres -> Sécurité -> Applications Administrateur</i> d\'appareil</string>
    <string name="lock_disabled_warning_accessibility_settings_text">WaveUp a dû désactiver l\'option \"verrouiller\" à cause de permissions <i>Service Accessibilité</i> manquantes.

\n\nVeuillez réactiver l\'option et configurer WaveUp en tant que <i>Service d\'accessibilité</i> une fois demandé.

\n\nSi cela ne marche pas, veuillez activer \"Verrouillage WaveUp\" dans <i>Paramètres -> Accessibilité</i>.</string>

    <string name="root_access_failed">Impossible d\'obtenir l\'accès root</string>

    <string name="uninstall_button">Désinstaller WaveUp</string>
    <string name="removed_device_admin_rights">Droits administrateur supprimés. Choisissez l\'option de verrouillage d\'écran pour les réactiver.</string>

    <!-- Phone permission dialog -->
    <string name="phone_permission_yes">Demande</string>
    <string name="phone_permission_no">Ignorer</string>
    <string name="phone_permission_explanation"><b>Permission Téléphone</b>

\n\nWaveUp utilise cette permissions exclusivement afin de savoir si un appel est en cours et se désactiver pendant l\'appel.

\n\nAucune information relative à l\'appel n\'est demandée ou enregistrée. Aussi, le fait qu\'un appel soit en cours n\'est connu que de WaveUp et n\'est jamais envoyé autre part.

\n\n<b>WaveUp ne passera ou ne gérera jamais d\'appels téléphoniques.</b>

\n\nNous vous recommandons d\'accorder cette permission, mais ce n\'est pas grave si vous ne le faites pas.</string>

    <!-- I put this array here instead of in arrays.xml because it has to be translated. -->
    <string-array name="sensor_cover_time_entries">
        <item>Immédiatement</item>
        <item>0,5 secondes</item>
        <item>1 seconde</item>
        <item>1,5 secondes</item>
        <item>2 secondes</item>
        <item>5 secondes</item>
    </string-array>

    <!-- Vibrate on lock -->
    <string name="pref_vibrate_on_lock_time_title">Vibrer avant le  verrouillage</string>
    <string name="pref_vibrate_on_lock_time_title_summary">Vous notifie d\'une détection de proximité afin d\'éviter de verrouiller le téléphone par accident.

\n\nDurée de vibration actuelle : %s</string>

    <string-array name="vibrate_on_lock_time_entries">
        <item>Ne pas vibrer</item>
        <item>10 ms</item>
        <item>20 ms</item>
        <item>50 ms</item>
        <item>200 ms</item>
        <item>1000 ms</item>
    </string-array>

    <string name="pref_number_of_waves">Nombre de mouvements</string>
    <string name="pref_number_of_waves_summary">Nombre de fois où il faut passer la main devant le capteur de proximité (recouvrir puis découvrir) pour réveiller le téléphone. Actuellement : %s.</string>

    <string-array name="number_of_waves_entries">
        <item>1 (couvrir, découvrir)</item>
        <item>2 (couvrir, découvrir, couvrir, découvrir)</item>
        <item>3 (couvrir, découvrir, couvrir, découvrir)</item>
    </string-array>

    <string name="privacy_policy_menu_item">Politique de Confidentialité</string>
    <string name="licenses_menu_item">Licences</string>
    <!-- Excluded apps from locking -->
    <string name="untitled_app">Application sans nom</string>
    <string name="exclude_apps_activity_title">Exclure des applications</string>
    <string name="excluded_apps">Applis exclues</string>
    <string name="not_excluded_apps">Applications non exclues</string>
    <string name="exclude_apps_text">
Il n\'y a actuellement aucune application exclue.

\n\nLes applications listées ici seront exclues et WaveUp ne verrouillera pas l\'écran si quelconque de ces applications est ouverte.

\n\nSélectionnez une application ci-dessous afin de l\'ajouter à la liste. Sélectionnez la à nouveau pour la retirer si vous changez d\'avis.</string>
    <string name="usage_access_explanation">
<b>Exclure des applications au verrouillage : Accès utilisation</b>

\n\nAfin de ne <i>pas</i> pouvoir verrouiller l\'écran si une application en particulier est ouverte, WaveUp a besoin de l\'<i>Accès utilisation</i>.

\n\nVoulez-vous accorder à WaveUp l\'Accès utilisation ? Vous devrez accorder les privilèges manuellement au prochain écran.</string>


    <!-- App shortcut -->
    <string name="lock_now">Verrouiller maintenant</string>

    <!-- Lack of proximity sensor -->
    <string name="missing_proximity_sensor_title">Capteur de proximité manquant</string>
    <string name="missing_proximity_sensor_text">
WaveUp ne peut fonctionner sans capteur de proximité. Soit votre appareil n\'en possède pas soit WaveUp n\'es pas en capacité d\'y accéder. Désolé !</string>
</resources>
