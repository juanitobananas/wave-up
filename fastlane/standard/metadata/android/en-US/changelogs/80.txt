Great news for Android 9 users! WaveUp now uses a new Accessibility Services method to lock your device, which makes the fingerprint unlock feature work again, yay!
You'll have to re-enable the 'lock' option from the app again.

New in 3.0.1-beta
★ Only Android 9: Fix 'unable to unlock using fingerprint' bug.
★ Remove 'revoke device admin' menu item.

New in 2.6.9
★ Add Persian (Iran) translation. Thank you very much for this Moh!
★ Remove 'open usage access apps' menu item.
