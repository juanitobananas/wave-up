New in 3.2.1
★ Update some translations.

New in 3.2.0
★ Add 'App Shortcut' to lock device on Android 7.1 and newer.
★ Update Italian translation.

New in 3.1.8
★ Add FAQ to menu.

New in 3.1.7
★ Add night mode.
★ Update some translations.
