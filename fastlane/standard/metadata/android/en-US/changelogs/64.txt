New in 2.5.2
★ Fixed small vibrator bug
★ Check root in background to avoid crash
★ Add German legal notice (Impressum)
★ Add link to new show-the-dev-some-love version :)

New in 2.4.3
★ Fixed bug: Show dialog if no proximity sensor is available and avoid crash.

New in 2.4.2
★ Fixed bug on some devices while initializing proximity sensor.

New in 2.4.1
★ Fixed bug introduced in 2.4.0 which wouldn't allow WaveUp to start properly on some Oreo devices.