New in 3.1.8
★ Add FAQ to menu.

New in 3.1.7
★ Add night mode.
★ Update some translations.

New in 3.1.6
★ Update some translations.
