WaveUp, yakınlık sensörünün üzerinden <i>el salladığınızda</i> <i>telefonunuzu uyandıran</i> - ekranı açan - bir uygulamadır.

Bu uygulamayı geliştirdim çünkü sadece saate göz atmak için güç düğmesine basmak istemiyordum - ki bunu telefonumda çok yapıyorum. Halihazırda tam olarak bunu yapan başka uygulamalar var - ve hatta daha fazlasını. <b>Harika</b> bir uygulama olan Gravity Screen On/Off'dan ilham aldım. Bununla birlikte, büyük bir özgür yazılımların hayranıyım ve mümkünse telefonuma özgür yazılımlar yüklemeye çalışıyorum. Bunu yapan özgür bir uygulama bulamadım, bu yüzden kendim yaptım. Eğer ilgileniyorsanız, koda bir göz atabilirsiniz:
https://gitlab.com/juanitobananas/wave-up

Ekranı açmak için elinizi telefonunuzun yakınlık algılayıcısının üzerinden geçirin. Buna <i>dalga modu</i> denir ve ekranınızın yanlışlıkla açılmasını önlemek için ayarlar ekranından devre dışı bırakılabilir.

Ayrıca, akıllı telefonunuzu cebinizden veya çantanızdan çıkardığınızda ekranı açacaktır. Buna <i>cep modu denir</i>ve ayarlar ekranından devre dışı bırakılabilir.

Bu modların her ikisi de varsayılan olarak etkindir.

Ayrıca yakınlık sensörünü bir saniye (veya belirli bir süre) kapatırsanız telefonunuzu kilitler ve ekranı kapatır. Bunun özel bir adı yoktur, ancak yine de ayarlar ekranında değiştirilebilir. Bu, varsayılan olarak etkin değildir.

Daha önce hiç yakınlık sensörü duymamış olanlar için: telefonda konuşurken kulağınızı koyduğunuz yere yakın bir yerde olan küçük bir şey. Neredeyse görünmüyor ve telefonunuzda arama yaparken ekranı kapatmasını söylemekten sorumludur.

<b>Kaldırma</b>

Bu uygulama, Cihaz Yöneticisi iznini kullanır. Bu nedenle WaveUp 'normal' sürümünü kaldıramazsınız.

Kaldırmak için, sadece açın ve menünün altındaki 'WaveUp Kaldır' düğmesini kullanın.

<b>Bilinen Sorunlar</b>

Ne yazık ki, bazı akıllı telefonlar yakınlık sensörünü dinlerken CPU'nun çalışmasına izin veriyor. Buna uyanma kilidi denir ve ciddi miktarda pil boşalmasına neden olur. Bu benim suçum değil ve bunu değiştirmek için hiçbir şey yapamam. Diğer telefonlar, yakınlık sensörünü dinlerken ekran kapatıldığında “uykuya geçecek”. Bu durumda, pil boşalması neredeyse sıfırdır.

<b>Gerekli Android İzinleri:</b>

▸ WAKE_LOCK ekranı açmak için
▸ USES_POLICY_FORCE_LOCK Cihazı kilitlemek için
▸ RECEIVE_BOOT_COMPLETED seçilirse önyüklemede otomatik olarak başlatılır
▸ READ_PHONE_STATE Çağrı sırasında WaveUp'ı askıya almak için

<b>Çeşitli notlar</b>

Bu şimdiye kadar yazdığım ilk Android uygulaması, bu yüzden dikkatli olun!

Bu aynı zamanda açık kaynak dünyasına yaptığım ilk küçük katkı. En sonunda!

Bana herhangi bir tür geri bildirim verebilir ya da herhangi bir şekilde katkıda bulunursanız çok sevinirim!

Okuduğunuz için teşekkürler! 

Açık kaynak taşları!!!

<b>Çeviri</b>

WaveUp'ı dilinize çevirmenize yardımcı olabilirseniz gerçekten harika olurdu (İngilizce versiyonu bile muhtemelen gözden geçirilebilir).
Transifex'te iki proje olarak tercüme edilebilir: https://www.transifex.com/juanitobananas/waveup/ ve https://www.transifex.com/juanitobananas/libcommon/.

<b>Teşekkürler</b>

Özel teşekkürlerim:

Bkz: https://gitlab.com/juanitobananas/wave-up/#acknowledgments