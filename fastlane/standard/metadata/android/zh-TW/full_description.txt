<i>喚醒您的手機</i> - WaveUp 是一款讓您向距離感應器<i>揮動時開啟螢幕</i>的應用程式。

開發這個只是為了避免看時間而按下電源鍵 - 我經常在自己的手機上做這種事。目前已經有許多類似軟體，甚至功能更豐富，我也曾被一款<b>很棒</b>的 App「重力螢幕鎖 - 開/關」所吸引。但是作為一位開源軟體的忠實粉絲，我會盡可能地在手機上嘗試各種免費自由軟體。在這之前並未找到同類型的開放原始碼軟體，所以我自己做了一個。
若您有興趣，可在此查看原始碼：https://gitlab.com/juanitobananas/wave-up

只要向手機的距離感應器揮動您的手就能開啟螢幕。稱為 <i>Wave 模式</i>，您可在設定中停用以避免意外開啟螢幕。

您也能從口袋或錢包拿出手機時開啟螢幕。稱為<i>口袋模式</i>，亦可在設定中停用。

以上功能預設皆為啟用。

它還能在您遮蔽距離感應器 1 秒 (或指定間隔) 後關閉及鎖定螢幕。這項功能尚未命名，仍可在設定中調整。預設為停用。

若您從未曉得什麼是距離感應器：一個藏於手機聽筒附近的微小零件，通常無法直接看見。它能在通話時讓手機關閉螢幕。

<b>解除安裝</b>

本應用程式使用裝置管理員權限，因此您無法以一般形式移除 WaveUp。

如要移除，只須將畫面捲動至底部並按下「解除安裝 WaveUp」。

<b>已知問題</b>

某些裝置會在監控距離感應器時運作 CPU 並消耗大量電力，也就是<i>喚醒鎖定 (WakeLock)</i>。很抱歉我無力解決，這也並非我所造成。其他裝置能在螢幕休眠的同時監控距離感應器，這個狀態下耗電量趨近 0。

<b>要求的 Android 權限：</b>

▸ WAKE_LOCK 用於開啟螢幕
▸ USES_POLICY_FORCE_LOCK 用於鎖定裝置
▸ RECEIVE_BOOT_COMPLETED 開機時自動啟動 (如已啟用)
▸ READ_PHONE_STATE 通話期間暫停 WaveUp

<b>其他事項</b>

這是我第一個創作的 Android 應用程式，所以請小心！

這也是我首次為開源世界的一點貢獻，終於！

非常歡迎您提供建議或任何形式的協助！

感謝您的閱讀！

開源萬歲！！！

<b>翻譯</b>

若您能將 WaveUp 翻譯成您的語言就更酷了 (甚至是由英文版修改)。
在 Transifex 上的 2 個翻譯計畫：https://www.transifex.com/juanitobananas/waveup/ 和 https://www.transifex.com/juanitobananas/libcommon/

<b>銘謝</b>

特別感謝

參閱：https://gitlab.com/juanitobananas/wave-up/#acknowledgments