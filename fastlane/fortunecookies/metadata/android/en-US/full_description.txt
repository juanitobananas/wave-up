WaveUp 🥠 Fortune Cookies is an app that <i>wakes up your phone</i> - switches the screen on - when you <i>wave</i> over the proximity sensor. As a plus, for this pro version, you get some cool cookie sayings.
It's mostly a way for you to show your appreciation for my work.

Please check out WaveUp before you buy this app. The waving functionality remains the same. With the pro version, you get the coolness of the fortune cookies :)

Here is the original WaveUp description:

I have developed this app because I wanted to avoid pressing the power button just to take a look at the watch - which I happen to do a lot on my phone. There are already other apps that do exactly this - and even more. I was inspired by Gravity Screen On/Off, which is a <b>great</b> app. However, I am a huge fan of open source software and try to install free software (free as in freedom, not only free as in free beer) on my phone if possible. I wasn't able to find an open source app that did this so I just did it myself. If you're interested, you can take a look at the code:
https://gitlab.com/juanitobananas/wave-up

Just wave your hand over the proximity sensor of your phone to turn the screen on. This is called <i>wave mode</i> and can be disabled in the settings screen in order to avoid accidental switching on of your screen.

It will also turn on the screen when you take your smartphone out of your pocket or purse. This is <i>called pocket mode</i> and can also be disabled in the settings screen.

Both of these modes are enabled by default.

It also locks your phone and turns off the screen if you cover the proximity sensor for one second (or a specified time). This does not have a special name but can nonetheless be changed in the settings screen too. This is not enabled by default.

For those who have never heard proximity sensor before: it is a small thingie that is somewhere near where you put your ear when you speak on the phone. You practically can't see it and it is responsible for telling your phone to switch off the screen when you're on a call.

<b>Uninstall</b>

This app uses the Device Administrator permission. Therefore you cannot uninstall WaveUp 'normally'.

To uninstall it, just open it and use the 'Uninstall WaveUp' button at the bottom of the menu.

<b>Known issues</b>

Unfortunately, some smartphones let the CPU on while listening to the proximity sensor. This is called a <i>wake lock</i> and causes considerable battery drain. This isn't my fault and I cannot do anything to change this. Other phones will "go to sleep" when the screen is turned off while still listening to the proximity sensor. In this case, the battery drain is practically zero.

<b>Required Android Permissions:</b>

▸ WAKE_LOCK to turn on the screen
▸ USES_POLICY_FORCE_LOCK to lock the device
▸ RECEIVE_BOOT_COMPLETED to automatically startup on boot if selected
▸ READ_PHONE_STATE to suspend WaveUp while on a call

<b>Miscellaneous notes</b>

This is the first Android app I have ever written, so beware!

This is also my first small contribution to the open source world. Finally!

I would love if you could give me feedback of any kind or contribute in any way!

Thanks for reading!

Open source rocks!!!

<b>Translations</b>

It would be really cool if you could help translate WaveUp to your language (even the English version could probably be revised).
It's available for translation as two projects on Transifex: https://www.transifex.com/juanitobananas/waveup/ and https://www.transifex.com/juanitobananas/libcommon/.

<b>Acknowledgments</b>

My special thanks to:

See: https://gitlab.com/juanitobananas/wave-up/#acknowledgments
