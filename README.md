# WaveUp

WaveUp is an Android app that *wakes up* your phone - switches the screen on - when you *wave* over the proximity sensor.

I have developed this app because I wanted to avoid pressing the power button just to take a look at the watch - which I happen to do a lot on my phone. There are already other apps that do exactly this - and even more. I was inspired by Gravity Screen On/Off, which is a **great** app. However, I am a huge fan of open source software and try to install free software (free as in freedom, not only free as in free beer) on my phone if possible. I wasn't able to find an open source app that did this so I just did it myself.

Just wave your hand over the proximity sensor of your phone to turn the screen on. This is called *wave mode* and can be disabled in the settings screen in order to avoid accidental switching on of your screen.

It will also turn on the screen when you take your smartphone out of your pocket or purse. This is called *pocket mode* and can also be disabled in the settings screen.

Both of these modes are enabled by default.

It also locks your phone and turns off the screen if you cover the proximity sensor for one second (or a specified time). This does not have a special name but can nonetheless be changed in the settings screen too. This is not enabled by default.

For those who have never heard proximity sensor before: it is a small thingie that is somewhere near where you put your ear when you speak on the phone. You practically can't see it and it is responsible for telling your phone to switch off the screen when you're on a call.

## Use of Accessibility Services:</b>

- Purpose: Starting with Android 9, WaveUp utilizes the Accessibility Services API to allow users to switch off the screen.
- Privacy and Security: WaveUp uses the Accessibility Services API exclusively for this purpose and only if the 'lock' option is enabled. No personal data is collected, stored, or shared through this service.
- Permission Scope: The Accessibility Services permission is strictly used for enabling the screen lock feature. It does not monitor or interact with any other aspect of device usage.

## Required Android Permissions:

- WAKE_LOCK to turn on the screen
- USES_POLICY_FORCE_LOCK to lock the device
- RECEIVE_BOOT_COMPLETED to automatically startup on boot if selected
- READ_PHONE_STATE to suspend WaveUp while on a call

## FAQ

There is a small FAQ [here](FAQ.md).

## Known issues

Unfortunately, some smartphones let the CPU on while listening to the proximity sensor. This is called a *wake lock* and causes considerable battery drain. This isn't my fault and I cannot do anything to change this. Most phones, however, will "go to sleep" when the screen is turned off while still listening to the proximity sensor, resulting in practically zero battery drain.

## Donating

If you feel like donating, you can do so by using Liberapay [![Donate](https://liberapay.com/assets/widgets/donate.svg)](https://liberapay.com/juanitobananas/).

Maybe bitcoin is more of your liking? Here you go: bc1qem2nsrd80s9wlm6z08qz7leet58vmj6ppa8x4z

## Uninstalling on Android 8 and older

This app uses the Device Administrator permission to provide enhanced functionality, which means you cannot uninstall WaveUp in the usual way.

To uninstall it, open the app and use the 'Uninstall WaveUp' button at the bottom of the menu. This will deactivate the Device Administrator permission and uninstall the app directly.

## Miscellaneous notes

At the beginning I was thinking on calling it *Jedi Hand App* because you can do a similar movement to the Jedi mind control trick to turn on your display. In the end I cowardly refrained from it: I read some scary articles - damn you Internet! - about legal issues with *Star Wars* trademarks.

This is the first Android app I have ever written, so beware!

This is also my first small contribution to the open source world. Finally!

I would love if you could give me feedback of any kind or contribute in any way!

Thanks for reading!

Open source rocks!!!

## Translations

It would be really cool if you could help translate WaveUp to your language (even the English version could probably be revised).
It's available for translation as two projects on Transifex: [waveup](https://www.transifex.com/juanitobananas/waveup/ "WaveUp on transifex")
and [libcommon](https://www.transifex.com/juanitobananas/libcommon/ "libcommon on transifex").

## Acknowledgements

My special thanks to:

- **Aitor Beriain**: Basque translation.
- **Albert Hetbsn**: Spanish translation.
- **amontero**: Spanish translation.
- **anejjar Elhoucine**: Arabic (Egypt) translation.
- **Antonio Tessio**: Italian translation.
- **askthedust**: French translation.
- **Cedric Octave**: French translation.
- **Chorus Pocus**: Brazilian Portuguese translation.
- **Christo Agveriandika**: Indonesian translation.
- **Fede Asd**: Italian translation.
- **Felipe Valencia**: Spanish translation.
- **Erdener**: Turkish translation.
- **Enrico Monese**: Italian translation.
- **Fatih Fırıncı**: Turkish translation.
- **G M**: German translation.
- **gidano** (sympda.blog.hu): Hungarian translation.
- **Ismael Omaña**: Spanish translation.
- **itunes870517**: Chinese (Taiwan) translation.
- **Jeroen**: Dutch translation.
- **ko_lo**: French translation.
- **Litten (砾天 顾)**: Chinese translation.
- **Miguel García**: Spanish translation.
- **Moh**: Persian (Iran) translation.
- **Muha Aliss**: Turkish translation.
- **Naofumi**: Japanese translation.
- **Nikola Gerogiev**: Bulgarian translation.
- **Nikita**: Russian translation
- **Ole Carlsen**: Danish translation.
- **Radiation Gamma**: Basque translation.
- **Rivo Zängov**: Estonian translation.
- **Sebastian Krause**: German translation.
- **Taras Shevchenko**: Ukrainian translation.
- **Tsuyoshi**: Japanese translation and some coding.
- **Ufuk Yayla**: Turkish translation.
- **Vlad**: Ukrainian translation.
- **Walter Klosse**: Czech translation.
- **Zzz Zzz**: Italian translation.
- **זלמן זלזניק**: Hebrew translation.

Please forgive me if I have forgotten somebody. Just let me know and I'll add you/them to the list.

Download it
-----------

[<img src="https://gitlab.com/juanitobananas/wave-up/raw/master/f-droid/get-it-on.png"
      alt="Get it on F-Droid"
      height="80">](https://f-droid.org/app/com.jarsilio.android.waveup)

[<img src="https://gitlab.com/juanitobananas/wave-up/raw/master/google-play-store/google-play-badge.png"
      alt="Get it on Google Play"
      height="80">](https://play.google.com/store/apps/details?id=com.jarsilio.android.waveup)

Legal Attributions
------------------

Google Play and the Google Play logo are trademarks of Google Inc.
